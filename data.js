
document.getElementById('getData').addEventListener('click', getData);
function getData() {
    // code to execte
    fetch('stor.json')
        .then(function (res) {
            return res.json();
        })
        .then(function (data) {
            let result = `<h2> User Info From sampleUser.json </h2>`;
            data.forEach((user) => {
                const { id, name, email, aboutme, urlbox } = user
                result += `
                            <div class="container">
                                <div class="card" style="width: 18rem;">
                                 <img class="card-img-top" src="${urlbox}" alt="Card image cap">
                                         <div class="card-body">
                                            <h5 class="card-title">${name}</h5>
                                                  <p class="card-text">${aboutme}</p>
                                                         <a href="#" class="btn btn-primary">${email}</a>
                                                     </div>
                                                    </div>
                                            </div>
            `;
                document.getElementById('result').innerHTML = result;
            });
        })
}
